<?php


namespace Grvoyt\TelegramHandler;


use Grvoyt\TelegramHandler\Exceptions\ThandlerException;
use GuzzleHttp\Client;
use Throwable;
use Config;

class TelegramHandler
{
	private $client;
	private $chat_id;

	public function __construct()
	{
		$api_token = Config::get('thandler.api_key',false);
		if(!$api_token) {
			throw new ThandlerException('Telegram api key not set',422);
		}

		if(!$chat_id = Config::get('thandler.chat_id',false)) {
			throw new ThandlerException('Telegram chat_id not set',422);
		} else {
			$this->chat_id = $chat_id;
		}

		$this->client = new Client([
			'base_uri' => "https://api.telegram.org/bot$api_token/"
		]);

		$sum = 1;
	}

	public function captureException(Throwable $exception)
	{

	}

	protected function sendMesssage(string $text)
	{
		$this->client->request(
			'POST',
			'sendMessage',
			[
				'form_params' => [
					'chat_id' => $this->chat_id,
					'text'    => $text,
				]
			]
		);
	}
}
