<?php


namespace Grvoyt\TelegramHandler;


use Illuminate\Support\ServiceProvider;

class TelegramHandlerServiceProvider extends ServiceProvider
{
	public function boot()
	{

	}
	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton('thandler', function ($app) {
			return new TelegramHandler();
		});

		$this->mergeConfigFrom(__DIR__ . '/../config/thandler.php', 'thandler');
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return ['thandler'];
	}

}
