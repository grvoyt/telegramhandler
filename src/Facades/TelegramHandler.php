<?php


namespace Grvoyt\TelegramHandler\Facades;


use Illuminate\Support\Facades\Facade;

class TelegramHandler extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'thandler';
	}
}
